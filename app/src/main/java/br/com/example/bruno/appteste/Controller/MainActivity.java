package br.com.example.bruno.appteste.Controller;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.example.bruno.appteste.R;

public class MainActivity extends AppCompatActivity {


    Button btnCadastrar = (Button) findViewById(R.id.btnCadastrar);

    private EditText edtEmailLogin;
    private EditText edtSenhaLogin;
    private Button btnLogar;

    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtEmailLogin = (EditText) findViewById(R.id.edtEmailCadastroUser);
        edtSenhaLogin = (EditText) findViewById(R.id.edtSenhaCadastroUser);

        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                if (edtEmailLogin.toString() == "teste@teste.com" && edtSenhaLogin.toString() == "teste"){
                    startActivity(intent);
                }else{
                    alertDialog.setMessage("Email ou senha incorretos");
                }
            }
        });


        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CadastroUserActivity.class);
                startActivity(intent);
            }
        });

    }


}
