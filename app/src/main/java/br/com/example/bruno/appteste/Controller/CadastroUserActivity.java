package br.com.example.bruno.appteste.Controller;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.example.bruno.appteste.Entity.Usuario;
import br.com.example.bruno.appteste.R;

/**
 * Created by bruno on 28/07/2016.
 */
public class CadastroUserActivity extends AppCompatActivity {

    private EditText editTextNome;
    private EditText editTextEmail;
    private EditText editTextSenha;
    private Button btnCadastrar;

    Usuario usuario = new Usuario();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_user);

        editTextNome = (EditText) findViewById(R.id.edtNomeCadastroUser);
        editTextEmail = (EditText) findViewById(R.id.edtEmailCadastroUser);
        editTextSenha = (EditText) findViewById(R.id.edtSenhaCadastroUser);

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario.setNomeUsuario(String.valueOf(editTextNome));
                usuario.setEmailUsuario(String.valueOf(editTextEmail));
                usuario.setSenhaUsuario(String.valueOf(editTextSenha));

                //futuramente instancia uma DAO de user e cadastra o usuario
            }
        });

    }
}
