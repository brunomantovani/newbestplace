package br.com.example.bruno.appteste.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import br.com.example.bruno.appteste.R;

/**
 * Created by bruno on 28/07/2016.
 */
public class HomeActivity extends AppCompatActivity {

    Button btnCadastroLocal;
    Button btnListarLocal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnCadastroLocal = (Button) findViewById(R.id.btnNovoLocal);
        btnListarLocal = (Button) findViewById(R.id.btnListaLocal);

        btnCadastroLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, CadastroLocalActivity.class);
                startActivity(intent);
            }
        });

        btnListarLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ListarLocalActivity.class);
                startActivity(intent);
            }
        });

    }
}
