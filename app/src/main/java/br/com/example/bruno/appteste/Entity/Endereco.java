package br.com.example.bruno.appteste.Entity;

/**
 * Created by bruno on 28/07/2016.
 */
public class Endereco {

    private Integer codEndereco;
    private String rua;
    private Integer numero;

    public Endereco(Integer codEndereco, String rua, Integer numero) {
        this.codEndereco = codEndereco;
        this.rua = rua;
        this.numero = numero;
    }

    public Endereco() {
    }

    public Integer getCodEndereco() {
        return codEndereco;
    }

    public void setCodEndereco(Integer codEndereco) {
        this.codEndereco = codEndereco;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Endereco{" +
                "codEndereco=" + codEndereco +
                ", rua='" + rua + '\'' +
                ", numero=" + numero +
                '}';
    }
}
