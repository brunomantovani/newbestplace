package br.com.example.bruno.appteste.Entity;

/**
 * Created by bruno on 28/07/2016.
 */
public class Local {

    private Integer codLocal;
    private String nomeLocal;
    private String descricaoLocal;
    private Double nota;
    private Endereco endereco;

    public Local(Integer codLocal, String nomeLocal, String descricaoLocal, Double nota, Endereco endereco) {
        this.codLocal = codLocal;
        this.nomeLocal = nomeLocal;
        this.descricaoLocal = descricaoLocal;
        this.nota = nota;
        this.endereco = endereco;
    }

    public Local() {
    }

    public Integer getCodLocal() {
        return codLocal;
    }

    public void setCodLocal(Integer codLocal) {
        this.codLocal = codLocal;
    }

    public String getNomeLocal() {
        return nomeLocal;
    }

    public void setNomeLocal(String nomeLocal) {
        this.nomeLocal = nomeLocal;
    }

    public String getDescricaoLocal() {
        return descricaoLocal;
    }

    public void setDescricaoLocal(String descricaoLocal) {
        this.descricaoLocal = descricaoLocal;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return "Local{" +
                "codLocal=" + codLocal +
                ", nomeLocal=" + nomeLocal +
                ", descricaoLocal='" + descricaoLocal + '\'' +
                ", nota=" + nota +
                ", endereco=" + endereco +
                '}';
    }
}
