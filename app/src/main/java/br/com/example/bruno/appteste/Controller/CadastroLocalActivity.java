package br.com.example.bruno.appteste.Controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.example.bruno.appteste.Entity.Endereco;
import br.com.example.bruno.appteste.Entity.Local;
import br.com.example.bruno.appteste.R;

/**
 * Created by bruno on 28/07/2016.
 */
public class CadastroLocalActivity extends AppCompatActivity {


    private EditText edtNomeLocal;
    private EditText edtDescricaoLocal;
    private EditText edtNotaLocal;
    private EditText edtRuaLocal;
    private EditText edtNumeroLocal;
    private Button btnCadastrarLocal;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_local);

        final Endereco endereco = new Endereco();
        final Local local = new Local();

        btnCadastrarLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                local.setNomeLocal(String.valueOf(edtNomeLocal));
                local.setDescricaoLocal(String.valueOf(edtDescricaoLocal));
                local.setNota(Double.valueOf(String.valueOf(edtNotaLocal)));
                local.getEndereco().setRua(String.valueOf(edtRuaLocal));
                local.getEndereco().setNumero(Integer.valueOf(String.valueOf(edtNumeroLocal)));
            }
        });
    }
}
